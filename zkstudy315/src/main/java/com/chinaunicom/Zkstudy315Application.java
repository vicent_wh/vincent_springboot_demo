package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zkstudy315Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy315Application.class, args);
	}
}
