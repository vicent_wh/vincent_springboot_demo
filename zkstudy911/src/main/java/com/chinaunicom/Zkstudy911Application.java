package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Zkstudy911Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy911Application.class, args);
		//new SpringApplicationBuilder(Application.class).web(true).run(args);
	}
}
