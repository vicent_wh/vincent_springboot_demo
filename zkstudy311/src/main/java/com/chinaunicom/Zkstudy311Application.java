package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zkstudy311Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy311Application.class, args);
	}
}
