package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Zkstudy9111Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy9111Application.class, args);
	}
}
