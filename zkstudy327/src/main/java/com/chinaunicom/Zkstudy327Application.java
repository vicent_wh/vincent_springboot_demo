package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zkstudy327Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy327Application.class, args);
	}
}
