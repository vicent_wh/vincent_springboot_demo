package com.chinaunicom.domain;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {

    @Select("SELECT * FROM USER_1 WHERE NAME = #{name}")
    User findByName(@Param("name") String name);

    @Insert("INSERT INTO USER_1(ID,NAME, AGE) VALUES(#{id}, #{name}, #{age})")
    int insert(@Param("id") Integer id, @Param("name") String name, @Param("age") Integer age);

    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age")
    })
    @Select("SELECT name, age FROM USER_1")
    List<User> findAll();

    @Update("UPDATE USER_1 SET age=#{age} WHERE name=#{name}")
    void update(User user);

    @Delete("DELETE FROM USER_1 WHERE id =#{id}")
    void delete(Long id);

    @Insert("INSERT INTO user(id, name, age) VALUES(#{id}, #{name}, #{age})")
    int insertByUser(User user);

    @Insert("INSERT INTO user(id, name, age) VALUES(#{id,jdbcType=INTEGER}), #{name,jdbcType=VARCHAR}, #{age,jdbcType=INTEGER})")
    int insertByMap(Map<String, Object> map);


}