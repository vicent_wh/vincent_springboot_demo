package com.chinaunicom.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author vincent
 * @version 1.0.0
 */
public interface UserRepository extends MongoRepository<User, Long> {

    User findByName(String username);

}
