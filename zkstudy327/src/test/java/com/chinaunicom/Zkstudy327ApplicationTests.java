package com.chinaunicom;

import com.chinaunicom.domain.User;
import com.chinaunicom.domain.UserMapper;
import com.chinaunicom.domain.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
//@Transactional
public class Zkstudy327ApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private UserMapper userMapper;

	/**
	 *测试mysql连接mybatis begin
	 * throws Exception@
	@Test
	@Rollback
	public void findByName() throws Exception {
		userMapper.insert(3, "AAA1", 20);
		User u = userMapper.findByName("AAA1");
		Assert.assertEquals(20, u.getAge().intValue());
	}

	 *测试mysql连接mybatis end */

	/**
 	*测试mongodb begin
 	* */

	@Autowired
	private UserRepository userRepository;

	@Before
	public void setUp() {
		userRepository.deleteAll();
	}

	@Test
	public void test() throws Exception {

		// 创建三个User，并验证User总数
		userRepository.save(new User(1L, "didi", 30));
		userRepository.save(new User(2L, "mama", 40));
		userRepository.save(new User(3L, "kaka", 50));
		Assert.assertEquals(3, userRepository.findAll().size());

		// 删除一个User，再验证User总数
		User u = userRepository.findOne(1L);
		userRepository.delete(u);
		Assert.assertEquals(2, userRepository.findAll().size());

		// 删除一个User，再验证User总数N
		u = userRepository.findByName("mama");
		userRepository.delete(u);
		Assert.assertEquals(1, userRepository.findAll().size());

	}
	/**
	 *测试mongodb end
	 * */

}
