package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Zkstudy9112Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy9112Application.class, args);
	}
}
