package com.chinaunicom;

import com.chinaunicom.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 程序猿vincent
 * @version 1.0.0
 * @blog http://blog
 *
 */
@Controller
public class HelloController {
    
    @ResponseBody
    @RequestMapping("/hello")
    public String hello() {
        return "Hello World";
    }

    @RequestMapping("/")
    public String index(ModelMap map) {

        /**
         * 直接打开html页面展现Hello World，但是启动程序后，访问http://localhost:8080/，
         * 则是展示Controller中host的值：http://index -> thymeleaf to host，做到了不破坏HTML自身内容的数据逻辑分离
         */
        map.addAttribute("host", "http://index -> thymeleaf to host");
        return "index";
    }

    /**
     * 下面2个方法是316的例子，统一异常处理
     * throwE 是抛出系统异常，默认会把message放到${exception.message}，在error.html打出；
     * @return
     * @throws Exception
     */
    @RequestMapping("/throwE")
    public String throwE() throws Exception {
        throw new Exception("发生错误");
    }

    /**
     * json() 抛出自定义异常，结果是数组，可以处理除了url，message等其他包括数据等的错误处理
     * @return
     * @throws MyException
     */
    @RequestMapping("/json")
    public String json() throws MyException {
        throw new MyException("发生错误2");
    }



}