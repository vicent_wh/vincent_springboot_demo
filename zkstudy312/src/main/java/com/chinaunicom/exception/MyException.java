package com.chinaunicom.exception;

/**
 * @author vincent
 * @version 1.0.0
 * @date 17/2/9 上午10:02.
 * @blog http://blog
 */
public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }

}
