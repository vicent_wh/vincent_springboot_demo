package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zkstudy312Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy312Application.class, args);
	}
}
