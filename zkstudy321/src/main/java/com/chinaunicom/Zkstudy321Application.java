package com.chinaunicom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zkstudy321Application {

	public static void main(String[] args) {
		SpringApplication.run(Zkstudy321Application.class, args);
	}
}
